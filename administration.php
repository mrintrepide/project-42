<?php
	include("function.php");
	
	$server = ConnectBDD();
	$error = '';
	
	if( isset($_POST['mdpadmin']) and (strlen($_POST['mdpadmin']) > 0) )
	{
		$mdp = sha1(md5($_POST['mdpadmin']));
		if(! mysql_query("UPDATE utilisateur SET motdepasse = '$mdp' WHERE identifiant = 'admin';"))
		{
			$error = '<div class="alert"><strong>Erreur pendant la modification du mot de passe !</strong></div>';
		}
	}
	
	if( isset($_POST['mdparbitre']) and (strlen($_POST['mdparbitre']) > 0) )
	{
		$mdp = sha1(md5($_POST['mdparbitre']));
		if(! mysql_query("UPDATE utilisateur SET motdepasse = '$mdp' WHERE identifiant = 'arbitre';"))
		{
			$error = '<div class="alert"><strong>Erreur pendant la modification du mot de passe !</strong></div>';
		}
	}
	
	if( isset($_POST['mdphote']) and (strlen($_POST['mdphote']) > 0) )
	{
		$mdp = sha1(md5($_POST['mdphote']));
		if(! mysql_query("UPDATE utilisateur SET motdepasse = '$mdp' WHERE identifiant = 'hote';"))
		{
			$error = '<div class="alert"><strong>Erreur pendant la modification du mot de passe !</strong></div>';
		}
	}
	
	DrawHead("Administration");
	DrawMenu();
	echo $error;

	if($_SESSION['type'] == 3)
	{
?>
<div class="hero-unit">
	<h3 class="pagination pagination-centered">Administration</h3>
	<div>
		<table style="width:100%;">
			<form action="" method="POST">
				<tr>
					<th>Mot de pass admin :</th>
					<td><input name="mdpadmin" type="password" placeholder="Mot de passe admin" class="span4">
					<button type="submit" class="btn">Envoyer</button></td>
				</tr>
			</form>
			<form action="" method="POST">
				<tr>
					<th>Mot de pass arbitre :</th>
					<td><input name="mdparbitre" type="password" placeholder="Mot de passe arbitre" class="span4">
					<button type="submit" class="btn">Envoyer</button></td>
				</tr>
			</form>
			<form action="" method="POST">
				<tr>
					<th>Mot de pass hote(esse) :</th>
					<td><input name="mdphote" type="password" placeholder="Mot de pass hote(esse)" class="span4">
					<button type="submit" class="btn">Envoyer</button></td>
				</tr>
			</form>
			<form action="" method="POST">
				<tr>
					<th>Reset :</th>
					<td><button type="submit" class="btn">Remise à zero</button></td>
				</tr>
			</form>
		</table>
	</div>
</div>
<?php
	}
	else
	{
?>
<div class="hero-unit">
	<strong>Vous n'avez pas accès a cette zone !</strong>
</div>
<?php
	}
	DrawFoot();
?>
