<?php

function DrawHead($title)
{
session_start();

echo '<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8">
		<title>'.$title.'</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">

		<!-- Le styles -->
		<link href="assets/css/bootstrap.css" rel="stylesheet">
		<style type="text/css">
			body {
				padding-top: 60px;
				padding-bottom: 40px;
				background-image:url("assets/img/autowp63.jpg");
				background-attachment: fixed;
				background-size: cover;
				background-position: center center;
			}
			.hero-unit {
				margin: 0 auto;
				max-width:700px;
				font-size:14px;
			}
		</style>
		<link href="assets/css/bootstrap-responsive.css" rel="stylesheet">

		<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.js"></script>
		<![endif]-->

		<!-- Fav and touch icons -->
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
		<link rel="shortcut icon" href="assets/ico/favicon.png">
	</head>
	
	<body>
		<div class="container">';
}

function DrawFoot()
{
echo '</div> <!-- /container -->
		<!-- Le javascript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="assets/js/jquery.js"></script>
		<script src="assets/js/bootstrap-transition.js"></script>
		<script src="assets/js/bootstrap-alert.js"></script>
		<script src="assets/js/bootstrap-modal.js"></script>
		<script src="assets/js/bootstrap-dropdown.js"></script>
		<script src="assets/js/bootstrap-scrollspy.js"></script>
		<script src="assets/js/bootstrap-tab.js"></script>
		<script src="assets/js/bootstrap-tooltip.js"></script>
		<script src="assets/js/bootstrap-popover.js"></script>
		<script src="assets/js/bootstrap-button.js"></script>
		<script src="assets/js/bootstrap-collapse.js"></script>
		<script src="assets/js/bootstrap-carousel.js"></script>
		<script src="assets/js/bootstrap-typeahead.js"></script>
		<script>
			$(".alert").alert();
		</script>
	</body>
</html>';
}

function DrawMenu()
{
$error = "";
echo '<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">
			<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="brand" href="index.php">Project 42</a>
			<div class="nav-collapse collapse">
				<ul class="nav">
					<li><a href="index.php">Accueil</a></li>
					<li><a href="classement.php">Classement</a></li>
					<li><a href="arbitrage.php">Arbitrage</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Administration <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="ajoutClub.php">Ajout des clubs</a></li>
							<li><a href="ajoutEquipe.php">Ajout des équipes</a></li>
							<li><a href="ajoutJoueur.php">Ajout des joueur</a></li>
							<li><a href="ajoutJeu.php">Ajout des jeux</a></li>
							<li><a href="administration.php">Modifier les mots de passe</a></li>
						</ul>
					</li>
				</ul>'.Auth().'</div><!--/.nav-collapse -->
		</div>
	</div>
</div>';
}

function ConnectBDD()
{
	$serveur = mysql_connect("localhost", "root", "pass-42");
	if($serveur)
	{
		$bdd = mysql_select_db("projet42", $serveur);
		if(!$bdd)
		{
			return 0;
		}
	}
	else
	{
		$error = '<div class="alert"><strong>Erreur de connexion MySQL !</strong></div>';
	}
	return $serveur;
}

function Auth()
{
	$ret = '<form class="navbar-form pull-right" action="" method="post">
				<input class="span2" name="user" type="text" placeholder="Identifiant">
				<input class="span2" name="pass" type="password" placeholder="Mot de passe">
				<button type="submit" class="btn">Connexion</button>
			</form>';
	if ( isset($_GET['deconnexion']) )
	{
		if($_GET['deconnexion'] == "true")
		{
			session_destroy();
		}
		header('Location:index.php');
	}
	if ( isset($_SESSION['type']) )
	{
		switch($_SESSION['type'])
		{
			case 1:
				$type = "hôte(sse) d'accueil";
				break;
			case 2:
				$type = "arbitre";
				break;
			case 3:
				$type = "administrateur";
				break;
			default:
				$type = "non connecté";
				break;
		}
		$ret = '<ul class="nav pull-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Connecté en tant que '.$type.' <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="?deconnexion=true">Déconnexion</a></li>
						</ul>
					</li>
				</ul>';
	}
	else
	{
		if(isset($_POST['user']) && isset($_POST['pass']))
		{
			$user = $_POST['user'];
			$pass = sha1(md5($_POST['pass']));
			$server2 = ConnectBDD();
			$resultat3 = mysql_query("SELECT * FROM utilisateur WHERE identifiant = '$user' AND motdepasse = '$pass'");
			while( $tab3 = mysql_fetch_array($resultat3) )
			{
				if( ($tab3["identifiant"] == $user) && ($tab3["motdepasse"] == $pass) )
				{
					$_SESSION['type'] = $tab3['type'];
				}
			}
			header('Location:index.php');
		}
	}
	return $ret;
}

?>
