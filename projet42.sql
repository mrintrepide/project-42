-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 01, 2014 at 06:19 PM
-- Server version: 5.5.37-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `projet42`
--

-- --------------------------------------------------------

--
-- Table structure for table `club`
--

CREATE TABLE IF NOT EXISTS `club` (
  `idClub` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`idClub`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- Dumping data for table `club`
--

INSERT INTO `club` (`idClub`, `nom`) VALUES
(0, 'Aucun'),
(1, 'top-fr');

-- --------------------------------------------------------

--
-- Table structure for table `equipe`
--

CREATE TABLE IF NOT EXISTS `equipe` (
  `idEquipe` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) COLLATE utf8_bin NOT NULL,
  `club` int(11) NOT NULL,
  `plaque` varchar(45) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`idEquipe`),
  KEY `club` (`club`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=7 ;

--
-- Dumping data for table `equipe`
--

INSERT INTO `equipe` (`idEquipe`, `nom`, `club`, `plaque`) VALUES
(1, '#yolo', 1, '255-CRS-93'),
(2, '#Ã§amarchepas', 0, ''),
(3, '#erreurphp', 0, ''),
(4, '#jai-oublier-ma-clef-usb', 0, ''),
(5, '#ilfaut-plus-de-5-equipes', 0, ''),
(6, '#phpception', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `jeu`
--

CREATE TABLE IF NOT EXISTS `jeu` (
  `idJeu` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) COLLATE utf8_bin NOT NULL,
  `type` int(11) NOT NULL,
  `score-mini` int(11) NOT NULL,
  PRIMARY KEY (`idJeu`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Dumping data for table `jeu`
--

INSERT INTO `jeu` (`idJeu`, `nom`, `type`, `score-mini`) VALUES
(1, 'tir-Ã -l-arc', 3, 5),
(2, 'hacking-de-porte', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `joueur`
--

CREATE TABLE IF NOT EXISTS `joueur` (
  `idJoueur` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) COLLATE utf8_bin NOT NULL,
  `prenom` varchar(45) COLLATE utf8_bin NOT NULL,
  `numEquipe` int(11) NOT NULL,
  PRIMARY KEY (`idJoueur`),
  KEY `numEquipe` (`numEquipe`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Dumping data for table `joueur`
--

INSERT INTO `joueur` (`idJoueur`, `nom`, `prenom`, `numEquipe`) VALUES
(1, 'mrs', 'yola', 1),
(2, 'mr', 'yolyu', 1);

-- --------------------------------------------------------

--
-- Table structure for table `score`
--

CREATE TABLE IF NOT EXISTS `score` (
  `numEquipe` int(11) NOT NULL,
  `numJeu` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `nbPenalite` int(11) NOT NULL,
  KEY `numEquipe` (`numEquipe`),
  KEY `numJeu` (`numJeu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `score`
--

INSERT INTO `score` (`numEquipe`, `numJeu`, `score`, `nbPenalite`) VALUES
(1, 1, 44, 0),
(2, 1, 45, 0),
(3, 1, 70, 0),
(4, 1, 2, 0),
(5, 1, 666, 0),
(6, 1, 65536, 0),
(1, 2, 48, 0);

-- --------------------------------------------------------

--
-- Table structure for table `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `idUtilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `identifiant` varchar(45) COLLATE utf8_bin NOT NULL,
  `motdepasse` varchar(45) COLLATE utf8_bin NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`idUtilisateur`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Dumping data for table `utilisateur`
--

INSERT INTO `utilisateur` (`idUtilisateur`, `identifiant`, `motdepasse`, `type`) VALUES
(1, 'admin', '90b9aa7e25f80cf4f64e990b78a9fc5ebd6cecad', 3),
(2, 'arbitre', '6883b3865e84c0d2b0c4e1b895623c38b72b4b6a', 2),
(3, 'hote', 'f384f23e98b529dec26962ef9f56296a2b9c0561', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `equipe`
--
ALTER TABLE `equipe`
  ADD CONSTRAINT `club_equipe` FOREIGN KEY (`club`) REFERENCES `club` (`idClub`);

--
-- Constraints for table `joueur`
--
ALTER TABLE `joueur`
  ADD CONSTRAINT `joueur_equipe` FOREIGN KEY (`numEquipe`) REFERENCES `equipe` (`idEquipe`);

--
-- Constraints for table `score`
--
ALTER TABLE `score`
  ADD CONSTRAINT `score_equipe` FOREIGN KEY (`numEquipe`) REFERENCES `equipe` (`idEquipe`),
  ADD CONSTRAINT `score_jeu` FOREIGN KEY (`numJeu`) REFERENCES `jeu` (`idJeu`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
