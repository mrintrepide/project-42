<?php
	include("function.php");
	
	$server = ConnectBDD();
	$error = '';
	if( isset($_POST['nom']) && ( strlen($_POST['nom']) > 0 ) )
	{
		$nom = $_POST['nom'];
		if( isset($_POST['club']) ) {
			$club = $_POST['club'];
		} else {
			$club = 0;
		}
		if( isset($_POST['plaque']) ) {
			$plaque = $_POST['plaque'];
		} else {
			$plaque = '';
		}
		if(! mysql_query("INSERT INTO equipe VALUES(0, '$nom', '$club', '$plaque')"))
		{
			$error = '<div class="alert"><strong>Erreur pendant l\'ajout de l\'équipe !</strong></div>';
		}
	}
	
	if( isset($_POST['del']) )
	{
		$del = $_POST['del'];
		if(! mysql_query("DELETE FROM equipe WHERE idEquipe = '$del'"))
		{
			$error = '<div class="alert"><strong>Erreur pendant la suppression de l\'équipe !</strong></div>';
		}
	}
	
	DrawHead("Equipe");
	DrawMenu();
	echo $error;
	if($_SESSION['type'] == 1 || $_SESSION['type'] == 3)
	{
?>
<div class="hero-unit">
	<h3 class="pagination pagination-centered">Ajout d'une équipe</h3>
	<div>
		<form action="" method="post">
			<table style="width:100%;">
				<tr>
					<th>Nom* :</th>
					<td><input name="nom" type="text" placeholder="Le nom de l'équipe" class="span6"></td>
				</tr>
				<tr>
					<th>Club :</th>
					<td>
						<select name="club" class="span6">
<?php
	if( $resultat = mysql_query("SELECT * FROM club") )
	{
		while( $tab = mysql_fetch_array($resultat) )
		{
			echo '<option value="'.$tab["idClub"].'">'.$tab["nom"].'</option>';
		}
	}
?>
						</select>
					</td>
				</tr>
				<tr>
					<th>Plaque :</th>
					<td><input name="plaque" type="text" placeholder="Numéro de la plaque de la voiture" class="span6"></td>
				</tr>
				<tr>
					<td></td>
					<td><button type="submit" class="btn btn-primary">Envoyer</button>
					<button type="reset" class="btn">Reset</button></td>
				</tr>
			</table>
		</form>
	</div>
	<div>
		<table class="table table-hover">
			<tr>
				<th>#</th>
				<th>Nom</th>
				<th>Club</th>
				<th>Immatriculation</th>
				<th>Suppression</th>
			</tr>
<?php
	if( $resultat = mysql_query("SELECT * FROM equipe") )
	{
		while( $tab = mysql_fetch_array($resultat) )
		{
			echo '<tr><form action="" method="POST">';
			echo '<td>'.$tab["idEquipe"].'</td>';
			echo '<td>'.$tab["nom"].'</td>';
			$cname = $tab['club'];
			if( $resultat2 = mysql_query("SELECT * FROM club WHERE idClub = '$cname'") )
			{
				while( $tab2 = mysql_fetch_array($resultat2) )
				{
					$cname = $tab2["nom"];
				}
			}
			echo '<td>'.$cname.'</td>';
			echo '<td>'.$tab["plaque"].'</td>';
			echo '<td><form action="" method="post"><input type="hidden" name="del" value="'.$tab["idEquipe"].'" /><button type="submit" class="btn">Supprimer</button></form></td>';
			echo '</tr>';
		}
	}
?>
		</table>
	</div>
</div>
<?php
	}
	else
	{
?>
<div class="hero-unit">
	<strong>Vous n'avez pas accès a cette zone !</strong>
</div>
<?php
	}
	DrawFoot();
?>
