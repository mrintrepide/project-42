<?php

	include("function.php");
	
	$server = ConnectBDD();
	$error = '';
	
	
	if( isset($_POST['nom']) && (strlen($_POST['nom']) > 0)
		&& isset($_POST['play_type']) && (strlen($_POST['play_type']) > 0)
		&& isset($_POST['Minscore']) && (strlen($_POST['Minscore']) > 0) )
	{

		$nom=$_POST['nom'];
		$play_type=$_POST['play_type'];
		$Minscore=$_POST['Minscore'];
		$requete="INSERT INTO jeu VALUES (0, '$nom', '$play_type','$Minscore') ;";
		$resultat=mysql_query($requete);
		
		if (!$resultat)
		{ 
			$error = '<div class="alert"><strong>Erreur pendant l\'ajout du jeu !</strong></div>';
		}
	}
	
	if( isset($_POST['del']) )
	{
		$del = $_POST['del'];
		if(! mysql_query("DELETE FROM jeu WHERE idJeu = '$del'"))
		{
			$error = '<div class="alert"><strong>Erreur pendant la suppression du jeu !</strong></div>';
		}
	}
	
	DrawHead("Jeu");
	DrawMenu();
	echo $error;

	if($_SESSION['type'] == 1 || $_SESSION['type'] == 3)
	{
?>
<div class="hero-unit">
	<h3 class="pagination pagination-centered">Ajout d'un Jeu</h3>
	<div>
		<form action="" method="post">
			<table style="width:100%;">
				<tr>
					<th>Nom :</th>
					<td><input name="nom" type="text" placeholder="Le nom du jeu" class="span6"></td>
				</tr>
				<tr>
					<th>Type :</th>
					<td>
						<SELECT name="play_type" size="1" class="span6">
							<OPTION value="1" select>temps</OPTION>
							<OPTION value="2">distance</OPTION>
							<OPTION value="3">points</OPTION>
						</SELECT>
					</td>
				</tr>
				<tr>
					<th>Score mini :</th>
					<td><input name="Minscore" type="text" placeholder="score minimal si présent" class="span6"></td>
				</tr>
				<tr>
					<td></td>
					<td><button type="submit" class="btn btn-primary">Ajouter</button>
					<button type="reset" class="btn">Reset</button></td>
				</tr>
		</form>
	</div>
	<div>
		<table class="table table-hover">
			<tr>
				<th>#</th>
				<th>Nom</th>
				<th>Type</th>
				<th>Score Mini</th>
				<th>Suppression</th>
			</tr>
<?php
	if( $resultat = mysql_query("SELECT * FROM jeu") )
	{
		while( $tab = mysql_fetch_array($resultat) )
		{
			echo '<tr><form action="" method="post">';
			echo '<td>'.$tab["idJeu"].'</td>';
			echo '<td>'.$tab["nom"].'</td>';
			echo '<td>'.$tab["type"].'</td>';
			echo '<td>'.$tab["score-mini"].'</td>';
			echo '<td><form action="" method="post"><input type="hidden" name="del" value="'.$tab["idJeu"].'" /><button type="submit" class="btn">Supprimer</button></form></td>';
			echo '</tr>';
		}
	}
?>
		</table>
	</div>

</div>
<?php
	}
	else
	{
?>
<div class="hero-unit">
	<strong>Vous n'avez pas accès a cette zone !</strong>
</div>
<?php
	}
	DrawFoot();
	
?>
