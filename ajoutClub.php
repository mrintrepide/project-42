<?php
	include("function.php");
	
	$server = ConnectBDD();
	$error = '';
	if( isset($_POST['nom']) && ( strlen($_POST['nom']) > 0 ) )
	{
		$nom = $_POST['nom'];
		if(! mysql_query("INSERT INTO club VALUES(0, '$nom')"))
		{
			$error = '<div class="alert"><strong>Erreur pendant l\'ajout du club !</strong></div>';
		}
	}
	
	if( isset($_POST['del']) )
	{
		$del = $_POST['del'];
		if(! mysql_query("DELETE FROM club WHERE idClub = '$del'"))
		{
			$error = '<div class="alert"><strong>Erreur pendant la suppression du club !</strong></div>';
		}
	}
	
	DrawHead("Club");
	DrawMenu();
	echo $error;
	if($_SESSION['type'] == 1 || $_SESSION['type'] == 3)
	{
?>
<div class="hero-unit">
	<h3 class="pagination pagination-centered">Ajout d'un club</h3>
	<div>
		<form action="" method="post">
			<table style="width:100%;">
				<tr>
					<th>Nom* :</th>
					<td><input name="nom" type="text" placeholder="Le nom de l'équipe" class="span6"></td>
				</tr>
				<tr>
					<td></td>
					<td><button type="submit" class="btn btn-primary">Envoyer</button>
					<button type="reset" class="btn">Reset</button></td>
				</tr>
			</table>
		</form>
	</div>
	<div>
		<table class="table table-hover">
			<tr>
				<th>#</th>
				<th>Nom</th>
				<th>Suppression</th>
			</tr>
<?php
	if( $resultat = mysql_query("SELECT * FROM club") )
	{
		while( $tab = mysql_fetch_array($resultat) )
		{
			if($tab["idClub"] != 0)
			{
				echo '<tr><form action="" method="POST">';
				echo '<td>'.$tab["idClub"].'</td>';
				echo '<td>'.$tab["nom"].'</td>';
				echo '<td><form action="" method="post"><input type="hidden" name="del" value="'.$tab["idClub"].'" /><button type="submit" class="btn">Supprimer</button></form></td>';
				echo '</tr>';
			}
		}
	}
?>
		</table>
	</div>
</div>
<?php
	}
	else
	{
?>
<div class="hero-unit">
	<strong>Vous n'avez pas accès a cette zone !</strong>
</div>
<?php
	}
	DrawFoot();
?>
